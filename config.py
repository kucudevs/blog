class Config(object):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_DEFAULT_SENDER = ('KUCU Blog', 'devstuff49@gmail.com')
    # MAIL_USE_SSL : True
    # MAIL_DEBUG
    # MAIL_MAX_EMAILS
    # MAIL_SUPPRESS_SEND
    # MAIL_ASCII_ATTACHMENTS
    # BLOG_POSTS_PER_PAGE = 6
    # USER_FOLLOWERS_PER_PAGE = 10

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    SQLALCHEMY_ECHO = True


class TestingConfig(Config):
    TESTING = True
    DEBUG = False


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
