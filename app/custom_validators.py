from wtforms.validators import ValidationError

class WordLength(object):
    def __init__(self, min=-1, max=-1, message=None):
        self.min = min
        self.max = max
        if not message:
            message = u'This Field must have %i to %i words.' %(min, max)
        self.message = message

    def __call__(self, form, field):
        l = len(field.data.split()) or 0
        if l < self.min or self.max != -1 and l > self.max:
            raise ValidationError(self.message+' You have %i words.' %(len(field.data.split())))

WordLength = WordLength