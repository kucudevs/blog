import sendgrid
import os
from sendgrid.helpers.mail import *

# from flask import render_template
# from app import mail
# from flask_mail import Message


# def send_email(to, subject, template, **kwargs):
#     msg = Message(subject, recipients=[to])
#     msg.body = render_template(template + '.txt', **kwargs)
#     mail.send(msg)

def send_email(_from, to, subject, content, **kwargs):
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(_from)
    to_email = Email(to)
    subject = subject
    content = Content("text/plain", content)
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())
    # print(response.status_code)
    # print(response.body)
    # print(response.headers)