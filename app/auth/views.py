from flask import render_template, flash, abort, redirect, url_for, request
from flask_login import login_user, logout_user, login_required, current_user

from . import auth
from app import db
from app.models import User, Post
from .forms import LoginForm, RegisterForm, ResetPasswordForm, EnterEmailForm, UpdatePasswordForm
from app.emails import send_email


@auth.route('/login', methods=['GET', 'POST'])
def login():
    # Remember to secure next
    next = request.args.get('next')
    if current_user.is_authenticated:
        return redirect(next or url_for('home.home_page'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            # flash('You have logged in successfully')
            return redirect(next or url_for('home.home_page'))
        else:
            flash('Invalid email or password', 'error')
    return render_template('auth/login.html', form=form, title='Login', active='login')


@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None:
            flash('This email is already in use', 'warning')
        else:
            user = User(firstname=form.firstname.data, lastname=form.lastname.data, email=form.email.data, phone=form.phone.data, password=form.password.data)
            db.session.add(user)
            db.session.commit()
            flash('You have registered successfully', 'info')

            token = user.generate_confirmation_token()
            # send_email(user.email, 'Confirm Your Account', 'auth/email/confirm', user=user, token=token)
            url = url_for('auth.confirm', token=token, _external=True)
            send_email('devstuff49@gmail.com', user.email, 'Confirm your account', 'To confirm your account please click on the following link:' +url)
            flash('A confirmation email has been sent to you by email.')

            return redirect(url_for('home.home_page'))
    return render_template('auth/register.html', form=form, title='Register', active='register')


@auth.route('/forgot-password', methods=['GET', 'POST'])
def forgot_password():
    if current_user.is_authenticated:
        flash('You are logged in already. Logout to reset password')
        return redirect(url_for('home.home_page'))

    form = EnterEmailForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            flash('Email does not exist')
        else:
            token = user.generate_reset_password_token()
            url = url_for('auth.reset_password', token=token, _external=True)
            send_email('devstuff49@gmail.com', user.email, 'Reset your Password', 'To reset your password please click on the following link:' +url)
            flash('A Password reset link has been sent to your email.')
            return redirect(url_for('home.home_page'))
    return render_template('auth/email/enter_email.html', form=form, title='Reset Password')


@auth.route('/password/reset/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        logout_user()
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user.reset_password(token, form.password.data):
            flash('Password reset successfull')
            return redirect(url_for('auth.login'))
        else:
            return redirect(url_for('home.home_page'))
    return render_template('auth/password/reset_password.html', form=form, title='Reset Password')


@auth.route('/password/update', methods=['GET', 'POST'])
@login_required
def update_password():
    form = UpdatePasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=current_user.email).first()
        if user.verify_password(form.old_password.data):
            user.password = form.new_password.data
            db.session.commit()
            flash('Password updated successfully')
            return redirect(url_for('home.home_page'))
        else:
            flash('Invalid Old Password')
    return render_template('auth/password/update_password.html', form=form, title='Update Password')



@auth.route('/account/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('home.home_page'))
    if current_user.confirm(token):
        flash('You have confirmed your account')
    else:
        flash('The confirmation link is invalid or has expired')
    return redirect(url_for('home.home_page'))

@auth.route('/delete/account')
def delete_account_initial_step():
    token = current_user.generate_confirmation_token()
    url = url_for('auth.delete_account_final_step', token=token, _external=True)
    send_email('devstuff49@gmail.com', current_user.email, 'Delete your account', 'To delete your account please click on the following link:' +url)
    flash('A delete-account link has been sent to your email')
    return redirect(url_for('home.home_page'))

@auth.route('/account/delete/<token>')
@login_required
def delete_account_final_step(token):
    if current_user.delete_account(token):
        flash('You have deleted your account')
    else:
        flash('The confirmation link is invalid or has expired')
    return redirect(url_for('home.home_page'))

@auth.route('/logout')
@login_required
def logout():
    """Logging out current user"""
    logout_user()
    flash('You have logged out successfully', 'info')

    # redirect to the login page
    return redirect(url_for('home.home_page'))


@auth.before_app_request
def before_request():
    # runs before every request from user. calls functions here before user request
    if current_user.is_authenticated:
        current_user.ping()
        # not 'auth.' so as to allow user access routes in the authentication blueprint
        if not current_user.confirmed and request.endpoint[:5] != 'auth.':
            return redirect(url_for('auth.unconfirmed'))


@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous or current_user.confirmed:
        return redirect('home.home_page')
    return render_template('auth/unconfirmed.html', title='Unconfirmed')


@auth.route('/confirm')
@login_required
def resend_confirmation():
    user = User.query.filter_by(email=current_user.email).first()
    token = user.generate_confirmation_token()
    url = url_for('auth.confirm', token=token, _external=True)
    send_email('devstuff49@gmail.com', user.email, 'Confirm your account', 'To confirm your account please click on the following link:'+ url)
    flash('A new confirmation email has been sent to your email')
    return redirect(url_for('home.home_page'))
