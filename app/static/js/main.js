function onlyThreeTags(select) {
  var result = [];
  var options = select && select.options;
  var opt;

  for (var i = 0, iLen = options.length; i < iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  if (result.length > 3) {
    alert('Select less that four tags')
  }
}
var selectTag = document.getElementById('tag')
if (selectTag) {
  selectTag.addEventListener('change', onlyThreeTags(selectTag))
}

var editor = new Quill('#editor-container', {
  bounds: '#editor-container',
  modules: {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['link', 'blockquote'],
      [{
        list: 'ordered'
      }, {
        list: 'bullet'
      }],
      [{
        'color': []
      }, {
        'background': []
      }],
      [{
        'size': ['small', false, 'large', 'huge']
      }],
      [{
        'font': []
      }],
      [{
        'align': []
      }],
      [{
        header: [1, 2, false]
      }]
    ]
  },
  placeholder: 'Enter the content of your article here (200 to 1000 words)...',
  theme: 'snow'
});

var form = document.querySelector('form');
if (form) {
  var modalBody = document.getElementById('modal-body');
  var body = document.querySelector('input[name=body]');
  if (body.value) {
    editor.root.innerHTML = body.value
    editor.root.innerHTML = modalBody.innerHTML
  }
  form.onsubmit = function () {
    // Populate hidden form on submit
    body.value = editor.root.innerHTML;
  };
}
