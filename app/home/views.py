import cloudinary
import hashlib
import os
import cloudinary.uploader
from flask import render_template, flash, redirect, url_for, request, current_app, abort, make_response, Markup as markup, send_from_directory
from flask_login import current_user, login_required
from werkzeug.utils import secure_filename

from app import db
from . import home
from app.models import User, Permission, Post, Comment, Tag, categories
from .forms import UpdateProfileForm, PostForm, CommentForm, ProfilePhotoForm
from app.decorators import permission_required
from app.emails import send_email

cloudinary = cloudinary

@home.route('/')
def home_page():
    page = request.args.get('page', 1, type=int)
    show_followed = False
    if current_user.is_authenticated:
        show_followed = bool(request.cookies.get('show_followed', ''))
    if show_followed:
        query = current_user.followed_user_posts
    else:
        if not current_user.is_administrator():
            query = Post.query.filter_by(approved=True)
        else:
            query = Post.query
    pagination = query.order_by(Post.timestamp.desc()).paginate(page, per_page = 6, error_out=False)
    posts = pagination.items
    return render_template('home/index.html', posts=posts, show_followed=show_followed, markup=markup, pagination=pagination, title='Home', active='home')


# ALL ROUTES RELATED TO TOPICS
@home.route('/topics')
def topics():
    topics = Tag.query.order_by(Tag.name).all()
    return render_template('home/topics.html', topics=topics, active='topics', title='Topics')


@home.route('/topics/<string:tag_name>')
def tag_posts(tag_name):
    """Route for viewing all posts under a topic/tag"""
    page = request.args.get('page', 1, type=int)
    tag = Tag.query.filter_by(name=tag_name.lower()).first()
    tagposts = tag.posts
    pagination = tagposts.order_by(Post.timestamp.desc()).paginate(page, per_page = 6, error_out=False)
    posts = pagination.items
    return render_template('home/tag_posts.html', posts=posts, markup=markup, tag_name=tag.name.lower(), pagination=pagination, title='Topic: {}'.format(tag.name), active='topics')

# END OF ALL ROUTES RELATED TO TOPICS

# ALL ROUTES RELATED TO POSTS

@home.route('/new_article', methods=['GET', 'POST'])
@login_required
def write_article():
    form = PostForm()
    form.tag.choices = [(tag.id, tag.name) for tag in Tag.query.order_by('name')]
    if current_user.can(Permission.WRITE_ARTICLES) and form.validate_on_submit():
        if os.getenv('FLASK_ENV') == 'production':
            # result = cloudinary.uploader.upload(form.post_image.data, proxy = "http://proxy.server:3128")
            result = cloudinary.uploader.upload(form.post_image.data)
        else:
            result = cloudinary.uploader.upload(form.post_image.data)
        post = Post(subject=form.subject.data, image_url=result['secure_url'],body=form.body.data, author=current_user._get_current_object())
        post = Post(subject=form.subject.data, body=form.body.data, author=current_user._get_current_object())
        for id in form.tag.data:
                post.tags.append(Tag.query.get(id))
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('.home_page'))
    return render_template('home/write_post.html', form=form, title='New Post')

@home.route('/post/<int:id>', methods=['GET', 'POST'])
def view_post(id):
    post = Post.query.get_or_404(id)
    if current_user is not post.author and not current_user.is_administrator() and post.is_approved() == False:
        abort(404)
    form = CommentForm()
    if current_user.is_authenticated and form.validate_on_submit():
        comment = Comment(body=form.body.data, post=post, author=current_user._get_current_object())
        db.session.add(comment)
        db.session.commit()
        return redirect(url_for('.view_post', id=post.id, page=-1))
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (post.comments.count() - 1)/6+1
    pagination = post.comments.order_by(Comment.timestamp.asc()).paginate(page, per_page=6, error_out=False)
    comments = pagination.items
    return render_template('home/post.html', markup=markup, posts=[post], form=form, comments=comments, pagination=pagination, title='Post')


@home.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_post(id):
    post = Post.query.get_or_404(id)
    if current_user != post.author and not current_user.can(Permission.ADMINISTER):
        abort(403)
    form = PostForm(obj=post)
    form.tag.choices = [(tag.id, tag.name) for tag in Tag.query.order_by('name')]
    if form.validate_on_submit():
        post.subject = form.subject.data
        if os.getenv('FLASK_ENV') == 'production':
            # result = cloudinary.uploader.upload(form.post_image.data, proxy = "http://proxy.server:3128")
            result = cloudinary.uploader.upload(form.post_image.data)
        else:
            result = cloudinary.uploader.upload(form.post_image.data)
        post.image_url = result['secure_url']
        post.body = form.body.data
        posttags = db.session.query(categories).filter_by(post_id=id).all()
        for id in form.tag.data:
            if (post.id, id) not in posttags:
                post.tags.append(Tag.query.get(id))
        db.session.commit()
        return redirect(url_for('.view_post', id=post.id))
    form.subject.data = post.subject
    # form.post_image.data = post.image_url
    form.body.data = post.body
    form.tag.default = [tag_id for tag_id in db.session.query(categories).filter_by(post_id=post.id).all()]
    form.process(obj=post)
    return render_template('home/write_post.html', markup=markup, form=form, post=post, title='Edit Post')


@home.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_post(id):
    post = Post.query.get_or_404(id)
    if current_user != post.author and not current_user.can(Permission.ADMINISTER):
        abort(403)
    Post.delete(post)
    return redirect(url_for('.home_page'))


@home.route('/all')
@login_required
def show_all_posts():
    resp = make_response(redirect(url_for('.home_page', active_pill='all')))
    resp.set_cookie('show_followed', '', max_age=30*24*60*60)
    return resp

@home.route('/followed_user_posts')
@login_required
def show_followed_user_posts():
    resp = make_response(redirect(url_for('.home_page')))
    resp.set_cookie('show_followed', '1', max_age=30*24*60*60)
    return resp

# END OF ALL ROUTES RELATED TO POSTS

# ALL ROUTES RELATED TO USER PROFILE

@home.route('/profile/<int:id>')
@login_required
def profile(id):
    # user = current_user
    user = User.query.get_or_404(id)
    posts = user.posts.order_by(Post.timestamp.desc()).all()
    return render_template('home/profile.html',markup=markup, user=user, id=user.id, posts=posts, title='Profile', active='profile')


@home.route('/profile/update', methods=['GET', 'POST'])
@login_required
def profile_update():
    form = UpdateProfileForm()
    if form.validate_on_submit():
        current_user.firstname = form.firstname.data
        current_user.lastname = form.lastname.data
        current_user.email = form.email.data
        current_user.phone = form.phone.data
        current_user.about_me = form.about_me.data
        current_user.avatar_hash = hashlib.md5(form.email.data.encode('utf-8')).hexdigest()
        db.session.commit()
        flash('Your profile has been updated.')
        return redirect(url_for('.profile', id=current_user.id))
    form.firstname.data = current_user.firstname
    form.lastname.data = current_user.lastname
    form.phone.data = current_user.phone
    form.email.data = current_user.email
    form.about_me.data = current_user.about_me
    
    return render_template('home/update_profile.html', form=form, title='Update Profile')

@home.route('/profile_photo/upload', methods=['GET', 'POST'])
def upload_profile_photo():
    form = ProfilePhotoForm()
    if form.validate_on_submit():
        file = form.profile_photo.data
        if os.getenv('FLASK_ENV') == 'production':
            # result = cloudinary.uploader.upload(file, proxy = "http://proxy.server:3128")
            result = cloudinary.uploader.upload(file)
        else:
            result = cloudinary.uploader.upload(file)
        current_user.profile_photo_path = result['secure_url']
        # filename = secure_filename(file.filename)
        # f = os.path.join(os.path.abspath('app/static/profile_photos'), filename)
        # file.save(f)
        # current_user.profile_photo_path = f
        db.session.commit()
        return redirect(url_for('.profile', id=current_user.id))

    return render_template('home/profile_photo_form.html', form=form, title='Profile photo')

home.route('/profile_pic/<path:filename>')
def profile_pic(filename):
        return send_from_directory('profile_photos', "filename", as_attachment=True)

# END OF ALL ROUTES RELATED TO USER PROFILE

# ALL ROUTES RELATED TO FOLLOWING

@home.route('/follow/<int:id>')
@login_required
@permission_required(Permission.FOLLOW)
def follow(id):
    user = User.query.get_or_404(id)
    if user is None:
        flash('Invalid User')
        return redirect(url_for('.home_page'))
    if current_user.is_following(user):
        flash('You are already following this user.')
        return redirect(url_for('.profile', id=id))
    current_user.follow(user)
    # flash('You are now following %s.' % user.firstname)
    return redirect(url_for('.profile', id=id))

@home.route('/followers/<int:id>')
@login_required
def followers(id):
    # This route shows all followers of a user
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.followers.paginate(page, per_page=10, error_out=False)
    followed_by = [{'user': item.follower, 'timestamp': item.timestamp} for item in pagination.items]
    return render_template('home/followers.html', user=user, id=user.id, endpoint='.followers', pagination=pagination, followed_by=followed_by)

@home.route('/following/<int:id>')
@login_required
def following(id):
    # This route shows all users followed by a user
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.followed.paginate(page, per_page=10, error_out=False)
    follows = [{'user': item.followed, 'timestamp': item.timestamp} for item in pagination.items]
    return render_template('home/following.html', user=user, id=user.id, endpoint='.following', pagination=pagination, follows=follows)

@home.route('/unfollow/<int:id>')
@login_required
def unfollow(id):
    user = User.query.get_or_404(id)
    if not current_user.is_following(user):
        flash('You are not following %s.' % user.firstname)
        return redirect(url_for('.profile', id=id))
    current_user.unfollow(user)
    return redirect(url_for('.profile', id=id))

# END OF ALL ROUTES RELATED TO FOLLOWING

# ALL ROUTES RELATED TO COMMENTS

@home.route('/edit_comment/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_comment(id):
    pass

@home.route('/delete_comment/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_comment(id):
    comment = Comment.query.get_or_404(id)
    if current_user.is_administrator() or current_user == comment.author:
        db.session.delete(comment)
        db.session.commit()
        return redirect(url_for('.moderate'))


@home.route('/moderate')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate():
    page = request.args.get('page', 1, type=int)
    pagination = Comment.query.order_by(Comment.timestamp.desc()).paginate(page, per_page=10, error_out=False)
    comments = pagination.items
    return render_template('home/moderate.html', comments=comments, pagination=pagination, page=page, title='Moderate Comments', active='moderate')

@home.route('/moderate/enable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_enable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled = False
    db.session.add(comment)
    db.session.commit()
    return redirect(url_for('.moderate',
page=request.args.get('page', 1, type=int)))

@home.route('/moderate/disable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_disable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled = True
    db.session.add(comment)
    return redirect(url_for('.moderate',
page=request.args.get('page', 1, type=int)))

# END OF ALL ROUTES RELATED TO COMMENTS