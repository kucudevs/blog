from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, TextAreaField, SelectMultipleField
from wtforms.validators import DataRequired, Email, Length
from flask_wtf.file import FileField, FileRequired

from app.custom_validators import WordLength

class UpdateProfileForm(FlaskForm):
    """Form for updating user profile"""
    firstname = StringField('Firstname', validators=[DataRequired()])
    lastname = StringField('Lastname', validators=[DataRequired()])
    email = email = StringField('Email', validators=[DataRequired(), Email()])
    phone = IntegerField('Phone Number', validators=[DataRequired()])
    about_me = TextAreaField('About Me', validators=[DataRequired()])
    submit = SubmitField('Submit')


class PostForm(FlaskForm):
    subject = StringField("Subject", validators=[DataRequired(), Length(min=4), WordLength(min=1, max=10, message='Subject must have 1 to 10 words')])
    post_image = FileField('Article Image')
    body = StringField("Body", description='(500 to 3000 words)', validators=[DataRequired(), WordLength(min=500, max=3000, message='Body must have 500 to 3000 words.')])
    tag = SelectMultipleField('Select 3 or less Tags (*Hold down the Ctrl Key to select many)', coerce=int, validators=[DataRequired(), Length(max=3, message='Please select less than 4 tags')])
    submit = SubmitField('Post')


class CommentForm(FlaskForm):
    body = StringField('', validators=[DataRequired()])
    submit = SubmitField('Comment')


class ProfilePhotoForm(FlaskForm):
    profile_photo = FileField(validators=[FileRequired()])
    submit = SubmitField('Save')