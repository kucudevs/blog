import hashlib
from flask import flash, url_for, redirect, render_template
from flask_login import login_required, current_user

from . import admin
from app import db
from app.models import User, Role, Tag, Post
from app.decorators import admin_required
from .forms import AdminEditProfileForm, AddTagForm

@admin.route('/profile/edit/user<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_userprofile(id):
    # This route is for editing the profiles of non-admins by admin
    user = User.query.get_or_404(id)
    form = AdminEditProfileForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.phone = form.phone.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        db.session.commit()
        flash('Profile has been updated')
        return redirect(url_for('home.profile', id=user.id))
    form.email.data = user.email
    form.phone.data = user.phone
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id

    return render_template('admin/edit_userprofile.html', id=user.id, form=form, user=user, title='Edit User Profile')


@admin.route('/add_tag', methods=['GET', 'POST'])
@login_required
@admin_required
def add_tag():
    form = AddTagForm()
    if form.validate_on_submit():
        tag = Tag(name=form.name.data, description=form.description.data)
        db.session.add(tag)
        db.session.commit()
        return redirect(url_for('home.home_page'))
    return render_template('admin/add_tag.html', form=form)


@admin.route('/approve_article/<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def approve_article(id):
    post = Post.query.get_or_404(id)
    if current_user.is_administrator and post.approved == False:
        post.approve()
        return redirect(url_for('home.view_post', id=post.id))