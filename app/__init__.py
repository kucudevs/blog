import os
import cloudinary

# third party imports
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_mail import Mail
from flask_moment import Moment
from flask_bootstrap import Bootstrap

# local imports
from config import app_config

# db variable initialization
db = SQLAlchemy(session_options={"expire_on_commit": False})

cloudinary = cloudinary
login_manager = LoginManager()
bootstrap = Bootstrap()
mail = Mail()
moment = Moment()

# application factory
def create_app(config_name):
    if os.getenv('FLASK_ENV') == 'production':
        app = Flask(__name__)
        app.config.update(
            SECRET_KEY=os.getenv('SECRET_KEY'),
            SQLALCHEMY_DATABASE_URI=os.getenv('SQLALCHEMY_DATABASE_URI'),
            MAIL_USERNAME=os.getenv('MAIL_USERNAME'),
            MAIL_PASSWORD=os.getenv('MAIL_PASSWORD'),
            BLOG_ADMIN=os.getenv('BLOG_ADMIN')
        )
        cloudinary.config(
            cloud_name=os.getenv('CLOUDINARY_CLOUD_NAME'),
            api_key=os.getenv('CLOUDINARY_API_KEY'),
            api_secret=os.getenv('CLOUDINARY_API_SECRET')
        )
    else:
        app = Flask(__name__, instance_relative_config=True)
        app.config.from_object(app_config[config_name])
        app.config.from_pyfile('config.py')

        cloudinary.config(
            cloud_name = app.config['CLOUDINARY_CLOUD_NAME'],
            api_key = app.config['CLOUDINARY_API_KEY'],
            api_secret = app.config['CLOUDINARY_API_SECRET']
            )

    db.init_app(app)
    migrate = Migrate(app, db)
    login_manager.init_app(app)
    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    login_manager.session_protection = 'strong'
    login_manager.login_message = "You must be logged in to access this page"
    login_manager.login_view = "auth.login"

    from app import models


    from app.admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from app.auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from app.home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    from .api_1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0')

    return app
